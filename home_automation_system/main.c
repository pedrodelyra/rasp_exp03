#include <stdio.h>
#include <include/uart.h>
#include <include/server.h>

int main(void) {
	printf("Home automation system started successfully.\n");
	start_server();
	return 0;
}
