#define TEMPERATURE_SENSOR_CODE 0xA1
#define PRESENCE_SENSOR_CODE 0xA2
#define WINDOWS_SENSOR_CODE 0xA3

#define AIR_CONDITIONING_DEVICE_CODE 0xB1
#define LAMP_DEVICE_CODE 0xB2
#define ALARM_DEVICE_CODE 0xB3

char uart_response_message[255];

int control_device(unsigned char target_device_id, char device_num, int command);

int read_all_devices(char* response);

int read_sensor(unsigned char target_sensor_id, char sensor_num);

int read_all_sensors(char* response);
