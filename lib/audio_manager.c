#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_BUFFER_SIZE 1000005

int file_size(FILE* file);

int play_audio(void) {
	FILE *audio_stream, *pipe;
	char audio_path[256], audio_buffer[MAX_BUFFER_SIZE];
	int audio_size;
	strcpy(audio_path, "audios/grenade.wav");
	if((audio_stream = fopen(audio_path, "rb")) == 0) {
		perror("Audio not found");
		exit(EXIT_FAILURE);
	}

	audio_size = file_size(audio_stream);
	fread(audio_buffer, sizeof(char), audio_size, audio_stream); 

	int pipe_fd;
	pipe = popen("aplay -t wav -", "w");
	pipe_fd = fileno(pipe);

	write(pipe_fd, audio_buffer, audio_size);
	pclose(pipe);
	fclose(audio_stream);
	return 0;
}

int file_size(FILE* file) {
	int size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	return size;
}
