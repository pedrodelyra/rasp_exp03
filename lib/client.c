#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT_1 4000
#define PORT_2 8080
#define EXIT_CODE 0

char user_options_messages[6][32] = {{"Read temperature sensors"    }, {"Read presence sensors"}, {"Read windows/doors sensors"},
				     {"Turn on/off air-conditioning"}, {"Turn on/off lamp"     }, {"Trigger/untrigger alarm"   }};
char auxiliar_messages[6][128] =
{{"1. External\n2. Internal bedroom\n3. Internal living room\n"},
 {"1. Main entrance\n2. Service entrance\n3. Garage\n"},
 {"1. Main door\n2. Service's door\n3. Garage's door\n4. Living room's window\n5. Bedroom's window\n6. Kitchen's window\n"},
 {"1. Living room\n2. Bedroom\n"},
 {"1. Main entrance\n2. Service entrance\n3. Garage\n4. Living room\n5. Bedroom\n6. Kitchen\n"},
 {"1. First sector (Presences sensors)\n2. Second sector (Doors/windows sensors)\n"}};

int code_to_socket(int n) {
	// map codes 610, 611 620 and 621 to first socket at port 4000
	return n < 610;
}

int display_menu(void) {
	int i, user_option = 0, auxiliar_option = 0, turn = 0;
	printf("Please, select one of the following options:\n");
	for(i = 0; i < 6; ++i) {
		printf("%d. %s\n", i + 1, user_options_messages[i]);
	}
	printf("0. Exit\n", i);

	scanf("%d", &user_option);
	if(user_option) {
		printf("%s", auxiliar_messages[user_option - 1]);
		scanf("%d", &auxiliar_option);
		if(user_option >= 4) {
			printf("Turn On / off: ");
			scanf("%d", &turn);
		}
	}

	return 100 * user_option + 10 * auxiliar_option + turn;
}

void build_request_message(int user_input, char* request_message) {
	sprintf(request_message, "%d", user_input);
	printf("Request message: %s\n", request_message);
}

int main(void) {
	int i, client_socket[2], server_port[2] = {PORT_1, PORT_2};
	struct sockaddr_in server_address[2];
	char *server_ip = "127.0.0.1", response_message[255], request_message[255]; int message_length;
	

	for(i = 0; i < 2; ++i) {
		if((client_socket[i] = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
			printf("Error while creating client's socket.\n");
		}

		memset(&server_address[i], 0, sizeof server_address[i]);

		server_address[i].sin_family = AF_INET;
		server_address[i].sin_addr.s_addr = inet_addr(server_ip);
		server_address[i].sin_port = htons(server_port[i]);
		if(connect(client_socket[i], (struct sockaddr *) &server_address[i], sizeof(server_address[i])) == -1) {
			printf("Error while establishing connection with server.\n");
			exit(EXIT_FAILURE);
		}

		printf("%dth client successfully connected with server.\n", i + 1);
	}

	int user_input, socket_id;
	while((user_input = display_menu()) != EXIT_CODE) {
		char request_message[35], response_message[35];
		memset(request_message, 0, sizeof request_message), memset(response_message, 0, sizeof response_message);
		build_request_message(user_input, request_message);
		socket_id = code_to_socket(user_input);
		printf("Sending request to %dth socket\n", socket_id);
		send(client_socket[socket_id], request_message, sizeof(request_message), 0);
		recv(client_socket[socket_id], response_message, sizeof(response_message), 0);
		printf("Client received response: %s\n", response_message);
	}
	for(i = 0; i < 2; ++i) {
		build_request_message(user_input, request_message);
		printf("Sending request with exit status to %dth socket\n", i + 1);
		send(client_socket[i], request_message, sizeof(request_message), 0);
		close(client_socket[i]);
		printf("Client is now exiting\n");
	}

	return 0;
}
