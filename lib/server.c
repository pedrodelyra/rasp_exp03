#ifndef __SERVER__
#define __SERVER__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <include/uart.h>
#include <include/audio_manager.h>

#define PORT_1 4000
#define PORT_2 8080

int protocol_code[] = {0, 0xA1, 0xA2, 0xA3, 0xB1, 0xB2, 0xB3};
sem_t semaphore;

void* manage_alarms(void* thread_args) {
	int server_socket = *(int *) thread_args, client_socket, client_length = sizeof(client_socket);
	struct sockaddr_in client_address;

	printf("Alarms sensor manager routine successfully started.\n");
	printf("Waiting for clients connections. . .\n");
	if((client_socket = accept(server_socket, (struct sockaddr *) &client_address, &client_length)) == -1) {
		printf("Failure on client's socket connection acception.\n");
		exit(EXIT_FAILURE);
	}
	printf("Connection established between client %s and alarms manager\n", inet_ntoa(client_address.sin_addr));

	while(1) {	
		char request_message[128], response_message[128];
		printf("Server (alarms manager) is waiting for clients requests. . .\n");
		recv(client_socket, request_message, 100, 0);	
		char target_device_id = protocol_code[request_message[0] - '0'],
					device_num = request_message[1] - '0', command = request_message[2] - '0';
		printf("Server (alarms manager) received request message: %s\n", request_message);
		if(*protocol_code == '0') break;
                sem_wait(&semaphore);
		control_device(target_device_id, device_num, command);
                sem_post(&semaphore);
		printf(command ? "Triggered alarm successfully\n" : "Untriggered alarm successfully\n");
		memcpy(response_message, command ? "Triggered alarm successfully\n" : "Untriggered alarm successfully\n", command ? 30 : 32);
		printf("Server (alarms manager) sending response to client: %s\n", response_message);
		send(client_socket, response_message, strlen(response_message), 0);
		printf("Server (alarms manager) sent response\n");
		printf("End of conversation between alarms manager and client\n");
	}

	close(client_socket);
	pthread_exit("Alarms manager thread routine is exiting.\n");
} 

void* manage_sensors(void* thread_args) {
	int server_socket = *(int *) thread_args, client_socket, client_length = sizeof(client_socket);
	struct sockaddr_in client_address;

	printf("Windows and doors sensor manager routine successfully started.\n");
	printf("Waiting for clients connections. . .\n");
	if((client_socket = accept(server_socket, (struct sockaddr *) &client_address, &client_length)) == -1) {
		printf("Failure on client's socket connection acception.\n");
		exit(EXIT_FAILURE);
	}
	printf("Connection established between client %s and windows and doors manager\n", inet_ntoa(client_address.sin_addr));

	while(1) {	
		char request_message[128], response_message[128];
		printf("Server (sensors manager) is waiting for clients requests. . .\n");
		recv(client_socket, request_message, 100, 0);	
		char target_device_id = protocol_code[request_message[0] - '0'],
					device_num = request_message[1] - '0', command = request_message[2] - '0';
		printf("Server (sensors manager) received request message: %s\n", request_message);
		if(*protocol_code == '0') break;
		if(target_device_id == AIR_CONDITIONING_DEVICE_CODE || target_device_id == LAMP_DEVICE_CODE) {
                	sem_wait(&semaphore);
			control_device(target_device_id, device_num, command);
                	sem_post(&semaphore);
			printf(command ? "Triggered device successfully\n" : "Untriggered device successfully\n");
			memcpy(response_message, command ? "Triggered device successfully\n" : "Untriggered device successfully\n", command ? 30 : 32);
			printf("Server (sensors manager) sending response to client: %s\n", response_message);
			send(client_socket, response_message, strlen(response_message), 0);
			printf("Server (sensors manager) sent response\n");
		} else {
			memset(uart_response_message, 0, sizeof uart_response_message);
                	sem_wait(&semaphore);
			read_sensor(target_device_id, device_num);
                	sem_post(&semaphore);
			printf("Server (sensors manager) sending response to client: %s\n", uart_response_message);
			send(client_socket, uart_response_message, strlen(uart_response_message), 0);
			printf("Server (sensors manager) sent response\n");
		} 

		printf("End of conversation between windows and doors manager and client\n");
	}

	close(client_socket);
	pthread_exit("Windows and doors manager thread routine is exiting.\n");
} 

void* control_first_sector_presence(void* args) {
	char response_message[25]; int i;
	while(1) {
                sem_wait(&semaphore);
		read_all_devices(response_message);
                sem_post(&semaphore);
		if(response_message[8]) {
			// Triggered alarm
			printf("First sector alarm is triggered.\n");
                	sem_wait(&semaphore);
			read_all_sensors(response_message);
                	sem_post(&semaphore);
			printf("First sector presence thread's response: ");
			for(i = 0; i < 10; ++i) { printf("(%d)", response_message[i]); } printf("\n");
			if(response_message[0] || response_message[1] || response_message[2]) {
				printf("First sector presence thread detected a presence. Alarm will be triggered!\n");
				play_audio();
			}
		}	

		sleep(2);
	}

	pthread_exit("First sector presence's thread is exiting\n");
}

void* control_second_sector_presence(void* args) {
	char response_message[25]; int i;
	while(1) {
                sem_wait(&semaphore);
		read_all_devices(response_message);
                sem_post(&semaphore);
		if(response_message[9]) {
			// Triggered alarm
			printf("Second sector alarm is triggered.\n");
                	sem_wait(&semaphore);
			read_all_sensors(response_message);
                	sem_post(&semaphore);
			printf("Second sector presence thread's response: ");
			for(i = 0; i < 10; ++i) { printf("(%d)", response_message[i]); } printf("\n");
			if(response_message[3] || response_message[4] || response_message[5] ||
			   response_message[6] || response_message[7] || response_message[8] || response_message[9]) {
				printf("Second sector presence thread detected a presence. Alarm will be triggered!\n");
				play_audio();
			}
		}	

		sleep(2);
	}

	pthread_exit("Second sector presence's thread is exiting\n");
}

int start_server(void) {
	int i, server_socket[2], server_port[2] = {PORT_1, PORT_2};
	struct sockaddr_in server_address[2];
	
	for(i = 0; i < 2; ++i) {
		if((server_socket[i] = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
			printf("Error in server's %dnd socket creation.\n", i);
			exit(EXIT_FAILURE);
		}

		memset(&server_address[i], 0, sizeof server_address[i]);
		server_address[i].sin_family = AF_INET;
		server_address[i].sin_addr.s_addr = htonl(INADDR_ANY);
		server_address[i].sin_port = htons(server_port[i]);

		if(bind(server_socket[i], (struct sockaddr *) &server_address[i], sizeof(server_address[i])) == -1) {
			printf("Error in assigning port number to %dnd server socket.\n", i);
			exit(EXIT_FAILURE);
		}

		if(listen(server_socket[i], 10) == -1) {
			printf("Error in creating request queue to server socket.\n");
			exit(EXIT_FAILURE);
		}
	}


	printf("Server successfully started.\n");
	pthread_t socket_thread[2], sensor_thread[2];
	void* (*server_threads[2])(void*) = {manage_alarms, manage_sensors};
	void* (*sensor_thread_routines[2])(void*) = {control_first_sector_presence, control_second_sector_presence};
	for(i = 0; i < 2; ++i) {
		pthread_create(&socket_thread[i], NULL, server_threads[i], &server_socket[i]);
		printf("%dth thread successfully created.\n", i + 1);
	}

        sem_init(&semaphore, 0, 1);
	for(i = 0; i < 2; ++i) {
		pthread_create(&sensor_thread[i], NULL, sensor_thread_routines[i], NULL);
	}

	for(i = 0; i < 2; ++i) {
		void *thread_result;
		pthread_join(socket_thread[i], &thread_result);
		printf("%s\n", (char *) thread_result);
		close(server_socket[i]);
	}
	
	return 0;
}

#endif
