#ifndef __UART__
#define __UART__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <include/uart.h>

int open_uart(void);
int set_uart_configurations(int);
size_t sensor_response_size(int);
void print_response_message(int, void*);

int open_uart(void) {
	int uart_file = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
	if(uart_file == -1) { perror("Error while openning uart"); exit(EXIT_FAILURE); }
	set_uart_configurations(uart_file);
	return uart_file;
}

int set_uart_configurations(int uart_fildes) {
	struct termios uart_options;
	tcgetattr(uart_fildes, &uart_options); 
	uart_options.c_cflag = B9600 | CS8 | CLOCAL | CREAD, uart_options.c_iflag = IGNPAR,
	uart_options.c_oflag = 0, uart_options.c_lflag = 0; 
	tcflush(uart_fildes, TCIFLUSH); tcsetattr(uart_fildes, TCSANOW, &uart_options);
}

int control_device(unsigned char target_device_id, char device_num, int command) {
	char request_message[3]; const int request_message_size = 3; int exit_status = 0;
	int uart = open_uart();
	request_message[0] = target_device_id, request_message[1] = device_num, request_message[2] = command;	
	printf("Sending request to arduino through uart: (%d)(%d)(%d)\n", request_message[0], request_message[1], request_message[2]);

	if(write(uart, request_message, request_message_size) == -1) {
		perror("Error while sending request to arduino");
		exit_status = -1;
	} else {
		printf("Request successfully sent to arduino.\n");
	}

	close(uart);
	return exit_status;
}

int read_all_devices(char* response) {
	unsigned char command = 0xA5;
	int uart = open_uart(), exit_status = 0;
	printf("Sending request to arduino through uart: (%d)\n", command);

	if(write(uart, &command, 1) == -1) {
		perror("Error while sending request to arduino");
		exit_status = -1;
	} else {
		char response_message[15] = {0};
		printf("Request successfully sent to arduino. Waiting for response.\n");
		sleep(1);
		if(read(uart, response_message, 10) == -1) {
			perror("Error while reading devices data.");
			exit_status = -1;
		} else {
			printf("Devices data successfully received: ");	
			int i;
			for(i = 0; i < 10; ++i) { printf("(%d)", response_message[i]); response[i] = response_message[i]; }
			printf("\n");
		}
	}

	close(uart);
	return exit_status;
}

int read_sensor(unsigned char target_sensor_id, char sensor_num) {
	char request_message[2]; const int request_message_size = 2; int exit_status = 0;
	int uart = open_uart();
	request_message[0] = target_sensor_id, request_message[1] = sensor_num;	
	printf("Sending request to arduino through uart: (%d)(%d)\n", request_message[0], request_message[1]);

	if(write(uart, request_message, request_message_size) == -1) {
		perror("Error while sending request to arduino");
		exit_status = -1;
	} else {
		char response_message[4]; const int response_message_size = sensor_response_size(target_sensor_id); void* result;
		printf("Request successfully sent to arduino. Waiting for response.\n");
		sleep(1);
		if(read(uart, response_message, response_message_size) == -1) {
			perror("Error while receiving response from arduino");
			exit_status = -1;
		} else {
			memcpy((char *) &result, response_message, response_message_size);
			printf("Response successfully received from arduino: ");
			print_response_message(target_sensor_id, &result);
		}	
	}

	close(uart);
	return exit_status;
}

int read_all_sensors(char* buffer) {
	unsigned char command = 0xA4;
	int uart = open_uart(), exit_status = 0;
	printf("Sending request to arduino through uart: (%d)\n", command);

	if(write(uart, &command, 1) == -1) {
		perror("Error while sending request to arduino");
		exit_status = -1;
	} else {
		char response_message[9] = {0}; int i;
		printf("Request successfully sent to arduino. Waiting for response.\n");
		sleep(1);
		if(read(uart, response_message, 9) == -1) {
			perror("Error while reading sensors data.");
			exit_status = -1;
		} else {
			printf("Sensors data successfully received: ");	
			for(i = 0; i < 9; ++i) { printf("(%d)", response_message[i]); }
			printf("\n");
			memcpy(buffer, response_message, strlen(response_message));
		}
	}

	close(uart);
	return exit_status;
}

size_t sensor_response_size(int sensor_id) {
	switch(sensor_id) {
		case TEMPERATURE_SENSOR_CODE:
			return 4;
		case PRESENCE_SENSOR_CODE:
		case WINDOWS_SENSOR_CODE:
			return 1;
	}

	return 0;
}

void print_response_message(int sensor_id, void* result) {
	switch(sensor_id) {
		case TEMPERATURE_SENSOR_CODE:
			printf("%.2lf\n", *(float *) result);
			sprintf(uart_response_message, "%.2lf", *(float *) result);
			break;
		case PRESENCE_SENSOR_CODE:
		case WINDOWS_SENSOR_CODE:
			printf("%d\n", *(char *) result);
			sprintf(uart_response_message, "%d", *(char *) result);
			break;
	}
}

#endif
